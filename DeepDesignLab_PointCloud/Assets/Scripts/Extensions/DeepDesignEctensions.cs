﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DeepDesignLab.Base {
    public static class DeepDesignEctensions {
       public static float[] Add(this float[] a, float[] b) {
            if (a.Length == b.Length) {
                float[] output = new float[a.Length];
                for (int i = 0; i < a.Length; i++) {
                    output[i] = a[i] + b[i];
                }
                return output;
            }
            return null;
        }
        public static void Multiply(this float[] a, float b) {
            for (int i = 0; i < a.Length; i++) {
                a[i] = a[i] * b;
            }
        }
    }
}
