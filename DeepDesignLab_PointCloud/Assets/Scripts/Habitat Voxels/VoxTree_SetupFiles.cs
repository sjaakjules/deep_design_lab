﻿using System.Collections;
using System.Collections.Generic;
using DeepDesignLab.Base;
using System.ComponentModel;
using UnityEngine;
using System;

namespace DeepDesignLab.PointCloud {
    public class VoxTree_SetupFiles : CCReader {
        bool fileReady = false;
        //VoxTreeData container;

        Dictionary<Vector2, List<double[]>> RawData = new Dictionary<Vector2, List<double[]>>();

        public Dictionary<Vector2, List<double[]>> getData { get { if (base.hasFinished) return RawData; return null; } }

        //TEMP VALUES
        Vector2 key;

        public VoxTree_SetupFiles() : base() {   //The Base() calls the parent constructor

            fileReady = true;

        }

        /*
       public VoxTreeFileReader(VoxTreeData newContainer):base() {   //The Base() calls the parent constructor

           container = newContainer;
           if (!(container.getNumberOfVoxels == 0)) {
               container.clearVoxels();
           }
          
        fileReady = true;  
            
        }
         */
        protected override void ProcessLineValues(double[] rowValues, string line, int lineNumber, BackgroundWorker worker, DoWorkEventArgs e) {
            if (fileReady) {

                key = new Vector2((float)Math.Round(rowValues[0], 0, MidpointRounding.AwayFromZero), (float)Math.Round(rowValues[1], 0, MidpointRounding.AwayFromZero));
                //key = new Vector3((float)Math.Round(rowValues[0],0,MidpointRounding.AwayFromZero),(float)Math.Round(rowValues[1],0, MidpointRounding.AwayFromZero),(float)Math.Round(rowValues[2], 0,MidpointRounding.AwayFromZero));
                if (RawData.ContainsKey(key)) {
                    RawData[key].Add(rowValues);
                }
                else {
                    RawData.Add(key, new List<double[]>());
                    RawData[key].Add(rowValues);
                }
                //container.ForceAddVoxel(new Voxel_Habitat(rowValues));      // CloudCompare data, {X,Y,Z,R,G,B,Scalar Field,Nx,Ny,Nz}
            }
            base.ProcessLineValues(rowValues, line, lineNumber, worker, e); // This is an empty function of the base class.
        }


    }

}

