﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System;
using System.ComponentModel;
using UnityEngine;
using UnityEditor;
using System.Text;
using Easy.Common.Extensions;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEditor.Experimental.VFX.Utility;
using Pcx;
using UnityEngine.VFX;
using UnityEditor.VFX;
using UnityEditor.VFX.UI;

namespace DeepDesignLab.PointCloud {
    public class VXFThreadedReader : MonoBehaviour {
        // Start is called before the first frame update
        //public string filePath;
        public System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();

        public VisualEffectAsset visualEffectAsset;
        public VisualEffect visualEffect;

        public BakedPointCloud[] CloudsToRender;
        VisualEffect[] newEffects;
        public int[] textureID = new int[3];
        public string[] textureName;

        public int nActive = 0;
        int lastActive = -1;

        void Start() {
            newEffects = new VisualEffect[CloudsToRender.Length];
            //filesToRead = Directory.GetFiles(filePath);
            for (int i = 0; i < CloudsToRender.Length; i++) {
                newEffects[i] = VisualEffect.Instantiate(visualEffect);
                newEffects[i].visualEffectAsset = visualEffectAsset;
                newEffects[i].SetTexture(Shader.PropertyToID("_Position"), CloudsToRender[i].positionMap);
                newEffects[i].SetTexture(Shader.PropertyToID("_Colour"), CloudsToRender[i].colorMap);
                newEffects[i].SetTexture(Shader.PropertyToID("_Normal"), CloudsToRender[i].normalMap);

                //newEffects[i].Reinit();
                newEffects[i].Stop();
              //  newEffects[i].pause = true;
               // newEffects[i].enabled = false;
                newEffects[i].gameObject.SetActive(false);

            }
        }

        // Update is called once per frame
        void Update() {
            if (lastActive != nActive)
            {
                timer.Start();
                for (int i = 0; i < newEffects.Length; i++)
                {
                    if (i == nActive)
                    {
                        if (newEffects[i].GetTexture(Shader.PropertyToID("_Position")) != CloudsToRender[i].positionMap)
                        {
                            newEffects[i].SetTexture(Shader.PropertyToID("_Position"), CloudsToRender[i].positionMap);
                            newEffects[i].SetTexture(Shader.PropertyToID("_Colour"), CloudsToRender[i].colorMap);
                            newEffects[i].SetTexture(Shader.PropertyToID("_Normal"), CloudsToRender[i].normalMap);
                        }
                        newEffects[i].gameObject.SetActive(true);
                        newEffects[i].Play();
                        //newEffects[i].enabled = true;
                       // newEffects[i].pause = false;
                    }
                    else
                    {

                        newEffects[i].Stop();
                      //  newEffects[i].pause = true;
                       // newEffects[i].enabled = false;
                        newEffects[i].gameObject.SetActive(false);
                    }
                }
                lastActive = nActive;
                timer.Stop();
                UnityEngine.Debug.Log(string.Format("File index {0}: {1}ms", lastActive, timer.ElapsedMilliseconds));
            }
        }

        /*
         private ManualResetEvent _doneEvent;
          string[] filesToRead;
           string readFilePath;

                public bool HaveFinished {
                    get {
                        for (int i = 0; i < doneEvents.Count; i++) {
                            if (!WaitHandle.WaitAll(doneEvents[i], 1)) {
                                return false;
                            }
                        }
                        return true;
                    }
                }

                List<ManualResetEvent[]> doneEvents = new List<ManualResetEvent[]>();
                public readBinaryFile[] readers;
                */
    }
}
